﻿using Emailer.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enterprise.Common;

namespace TemplateEmailer.Service.Models
{
    public class DebugMailer : SmtpMailer
    {
        public DebugMailer(ILogger logger) : base(logger)
        {
            this.IsHtml = true;
            SmtpHost = "localhost";
            SmtpPort = 25;
        }
    }
}