﻿using Emailer.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enterprise.Common;

namespace TemplateEmailer.Service.Infrastructure
{
    public class JncbRelayEmailer : SmtpMailer
    {
        public JncbRelayEmailer(ILogger logger) : base(logger)
        {
            this.IsHtml = true;
            //this.SmtpHost = Properties.Settings.Default.SmtpHost;
            //this.SmtpPort = Properties.Settings.Default.SmtpPort;
            //this.EnableSsl = false;
            //this.Password = Properties.Settings.Default.FromPassword;
        }
    }
}