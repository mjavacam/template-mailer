[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(TemplateEmailer.Service.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(TemplateEmailer.Service.App_Start.NinjectWebCommon), "Stop")]

namespace TemplateEmailer.Service.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using System.Web.Http;
    using Ninject.Web.WebApi;
    using Enterprise.Common;
    using JNCBTemplateMailer.Logger;
    using Emailer.Component;
    using Models;
    using Service;
    using Infrastructure;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper _bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            _bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            _bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                

                RegisterServices(kernel);
                
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            // Enable WebAPi
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
            kernel.Bind<ILogger>().To<FileLogger>();
            kernel.Bind<TemplateEngine.Component.TemplateStoreReader>().ToSelf();
            kernel.Bind<TemplateEngine.Component.TemplateBuilder>().ToSelf();
            kernel.Bind<TemplateEngine.Component.TemplateEngine>().ToSelf();
            kernel.Bind<IMailer>().To<JncbRelayEmailer>();
            kernel.Bind<IEmailerService>().To<SmtpEmailerService>();
            kernel.Bind<PDFGenerator.Component.HtmlPdfEngine>().ToSelf();
        }        
    }
}
