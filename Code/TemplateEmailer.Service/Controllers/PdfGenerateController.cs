﻿using Enterprise.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TemplateEmailer.Service.Models;
using Engine = TemplateEngine.Component;
using PDF = PDFGenerator.Component;

namespace TemplateEmailer.Service.Controllers
{
    //[RoutePrefix("pdf")]
    public class PdfController : ApiController
    {
        private readonly ILogger _logger;
        private readonly Engine.TemplateEngine _templateEngine;
        private readonly PDF.HtmlPdfEngine _htmlPdfEngine;

        public PdfController(ILogger logger, Engine.TemplateEngine templateEngine, PDF.HtmlPdfEngine htmlPdfEngine)
        {
            _logger = logger;
            _templateEngine = templateEngine;
            _htmlPdfEngine = htmlPdfEngine;
        }

        // POST: api/pdf/generate
        //[Route("generate")]
        public object Post([FromBody]Request request)
        {
            var response = new { status = HttpStatusCode.BadRequest };
            // Step 1 Parse Request
            _templateEngine.Data = request.Data;
            try
            {
                // Step 2 Locate Template by Id - Query Store
                _templateEngine.Execute(request.TemplateId);

                // Step 3 Locate
                if (_templateEngine.HasTemplate)
                {
                    GeneratePDF();

                    response = new { status = HttpStatusCode.OK };
                }
            }
            catch (Exception e)
            {
                _logger.Information("Unable to generate PDF");
                _logger.Error(e?.Message ?? e?.InnerException?.Message);
                response = new { status = HttpStatusCode.BadRequest };
            }

            return response;
        }

        private void GeneratePDF()
        {
            var fileNameString = Guid.NewGuid();
            _htmlPdfEngine.Generate(HttpContext.Current.Server.MapPath(@"~/Content/GeneratedFiles/"), $@"{fileNameString}.pdf", _templateEngine.CompiledTemplate);
        }
    }
}
