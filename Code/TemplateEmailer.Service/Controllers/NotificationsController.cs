﻿using Enterprise.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TemplateEmailer.Service.Models;
using Engine = TemplateEngine.Component;
using PDF = PDFGenerator.Component;

namespace TemplateEmailer.Service.Controllers
{
    public class NotificationsController : ApiController
    {
        private readonly IEmailerService _emailSrv;
        private readonly ILogger _logger;
        private readonly Engine.TemplateEngine _templateEngine;

        public NotificationsController(ILogger logger, IEmailerService emailService, Engine.TemplateEngine templateEngine)
        {
            _logger = logger;
            _emailSrv = emailService;
            _templateEngine = templateEngine;
        }

        // POST api/<controller>
        public object Post([FromBody]Request request)
        {
            var response = new { status = HttpStatusCode.BadRequest };
            // Step 1 Parse Request
            _templateEngine.Data = request.Data;
            try
            {
                // Step 2 Locate Template by Id - Query Store
                _templateEngine.Execute(request.TemplateId);

                // Step 3 Locate
                if (_templateEngine.HasTemplate)
                {
                    _emailSrv.SendEmail(Properties.Settings.Default.FromEmail, request.Recipient, request.Subject, _templateEngine.CompiledTemplate, true);
                    response = new { status = HttpStatusCode.OK };
                }
            }
            catch (Exception e)
            {
                _logger.Information("Unable to Send email");
                _logger.Error(e?.Message ?? e?.InnerException?.Message);
                response = new { status = HttpStatusCode.BadRequest };
            }

            return response;
        }
    }
}