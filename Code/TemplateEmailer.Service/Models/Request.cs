﻿using Newtonsoft.Json.Linq;

namespace TemplateEmailer.Service.Models
{
    public class Request
    {
        public string TemplateId { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public JObject Data { get; set; }

    }
}