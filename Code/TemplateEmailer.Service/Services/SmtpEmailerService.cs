﻿using Emailer.Component;
using Enterprise.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace TemplateEmailer.Service
{
    public class SmtpEmailerService : IEmailerService
    {

        string smtpPassword = string.Empty;
        private readonly ILogger _logger;
        private IMailer _mailer;

        public SmtpEmailerService(ILogger logger, IMailer mailer)
        {
            _logger = logger;
            _mailer = mailer;

        }
        public void SendEmail(string from, string to, string subject, string message)
        {
             this.SendEmail(from, to, subject, message, isHtml: false);
        }

        public void SendEmail(string from, string to, string subject, string message, bool isHtml = false)
        {
            _mailer.To = to;
            _mailer.From = from;
            _mailer.Subject = subject;
            _mailer.IsHtml = isHtml;
           // _mailer.EnableSsl = true;
            bool isSent = _mailer.SendAsync(message);
            if (!isSent)
            {
                _logger.Error($@"Email to: {to} was not sent");

            }
            else
            {
                _logger.Information($@"Email was successfully sent to {to}");
            }
        }

        public void Dispose()
        {
            _mailer.Dispose();
        }
    }
}