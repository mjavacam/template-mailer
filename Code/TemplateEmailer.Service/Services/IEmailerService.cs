﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TemplateEmailer.Service
{
    public interface IEmailerService : IDisposable
    {
        void SendEmail(string from, string to,string subject, string message);
        void SendEmail(string from, string to, string subject,string message, bool isHtml = false);
    }
}