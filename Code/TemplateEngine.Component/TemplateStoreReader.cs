﻿using Enterprise.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace TemplateEngine.Component
{
    public class TemplateStoreReader
    {
        private readonly ILogger _logger;

        public TemplateStoreReader(ILogger logger)
        {
            _logger = logger;
        }
        public string FilePath { get; private set; }
        public string Read(string id)
        {
            FilePath = HttpContext.Current.Server.MapPath($@"~/App_Data/EmailTemplates/{id}/{id}.txt");
            string templateMessage = string.Empty;
            if (CheckIfTemplateExists(FilePath))
            {
                _logger.Information($"Template: {id} was found and attempting to read.");
                try
                {
                    using (StreamReader streamReader = new StreamReader(FilePath))
                    {
                        templateMessage = streamReader.ReadToEnd();
                        _logger.Information($"Completed reading template {id}");
                    }
                }
                catch (Exception e)
                {

                    _logger.Error($"{e?.Message ?? e?.InnerException?.Message}");
                }
            }
            return templateMessage;
        }

        public bool CheckIfTemplateExists(string path)
        {
            if (!HasExtension(path))
            {
                // Has No Extension
                _logger.Warning("File has no extension");
                return false;
            }
            if (!IsValid(path))
            {
                // Invalid File
                _logger.Warning("Invalid file was passed");
                return false;
            }
            return File.Exists(path);

        }

        private bool IsValid(string file)
        {
            return Regex.IsMatch(file, @"^.*\.(txt)$");
        }

        private bool HasExtension(string path)
        {
            return Path.HasExtension(path);
        }
    }
}
