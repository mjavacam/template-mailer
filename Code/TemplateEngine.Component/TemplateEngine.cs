﻿using Enterprise.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TemplateEngine.Component
{
    public class TemplateEngine
    {
        private ILogger _logger;
        private TemplateBuilder _builder;
        public TemplateEngine(ILogger logger)
        {
            _logger = logger;
        }

        public string FilePath { get; private set; }
        public JObject Data { get; set; }
        public bool HasTemplate
        {
            get
            {
                return !String.IsNullOrWhiteSpace(CompiledTemplate);
            }
        }

        public string CompiledTemplate { get; set; }
        public void Execute(string id)
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();

            foreach (var item in Data.Children())
            {
                var prop = item as JProperty;
                var name = prop.Name;
                var value = prop.Value;
                parameters.Add(new KeyValuePair<string, string>(name, value.ToString()));
            }

                //(Data as IEnumerable<KeyValuePair<string,string>>)?.ToList();

            _builder = new TemplateBuilder(_logger);

            CompiledTemplate = _builder.BuildTemplate(id, parameters);


        }

        private List<KeyValuePair<string, string>> GetParametersKeyValues()
        {
            return Data.GetAllProperties()
                .Select(p => new KeyValuePair<string, string>(p.Name.ToCamelCase(), Data.GetPropertyValue(p.Name)))
                .ToList();
        }

        private List<KeyValuePair<string, string>> GenerateParameters(string id)
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            FilePath = HttpContext.Current.Server.MapPath($@"~/App_Data/EmailTemplates/{id}/{id}.xml");
            XDocument document = XDocument.Load(FilePath);
            var keyPairs = from parameter in document.Root.Descendants("Parameters").Elements()
                           select new KeyValuePair<string, string>(parameter.Element("Key").Value, parameter.Element("Value").Value);
            if (keyPairs.Count() > 0)
                parameters.AddRange(keyPairs);
            return parameters;
        }
    }
}
