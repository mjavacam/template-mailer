﻿using Enterprise.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateEngine.Component
{
    public class TemplateBuilder
    {
        private readonly ILogger _logger;
        private readonly TemplateStoreReader _reader;
        private StringBuilder _builder;
        public TemplateBuilder(ILogger logger)
        {
            _logger = logger;
            _reader = new TemplateStoreReader(_logger);
        }
        public string BuildTemplate(string id, ICollection<KeyValuePair<string, string>> parameters)
        {
            var template = _reader.Read(id);
            _builder = new StringBuilder(template);
         
            if (parameters.Count > 0)
            {
                _logger.Information("Replacing params for template.");
                foreach (var parameter in parameters)
                {
                    _builder = _builder.Replace($"{{{parameter.Key}}}", parameter.Value);
                    _logger.Information($@"Replacing <{parameter.Key}> with {parameter.Value}..");
                }
                _logger.Information("Completed replacing parameters.");
            }
            else
            {
                _logger.Warning("Collections are empty.");
            }

            return _builder.ToString();
        }
    }

}
