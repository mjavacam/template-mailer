﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;

namespace TemplateEngine
{
    public static class ModelParserExtensions
    {

        public static PropertyInfo[] GetAllProperties(this object model)
        {
            Type type = model.GetType();
            var props = type.GetProperties();
            return props;
        }

        public static string GetPropertyValue(this object @type, string propertyName)
        {
            Type t = @type.GetType();
            var prop = t.GetProperties().Where(p => p.Name == propertyName).Single();
            var target = Activator.CreateInstance(t);
            return Convert.ToString(prop.GetValue(target));
        }

        public static string ToPascalCase(this string given)
        {
            TextInfo info = Thread.CurrentThread.CurrentCulture.TextInfo;
            given = info.ToTitleCase(given);
            string[] parts = given.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
            string result = String.Join(String.Empty, parts);
            return result;
        }
        // Write custom extension methods here. They will be available to all queries.
        public static string ToCamelCase(this string given)
        {

            if (given == null || given.Length < 3) return given.ToLower();

            string result = given.ToPascalCase();
            result = given.Substring(0, 1).ToLower() + given.Substring(1);
            return result;
        }

    }
}