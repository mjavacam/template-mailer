﻿using Enterprise.Common;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFGenerator.Component
{
    public class HtmlPdfEngine
    {
        private readonly ILogger _logger;

        public HtmlPdfEngine(ILogger logger)
        {
            _logger = logger;
        }


        public string FilePath { get; private set; }
        public string FileName { get; private set; }
        public void Generate(string path, string fileName, string htmlText)
        {
            MapFileToPath(path, fileName);
            try
            {
                _logger.Information($@"Attempting to create PDF for {fileName}");
                using (FileStream fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    _logger.Information($@"Preparing data..");
                    using (Document document = new Document(PageSize.A4))
                    {
                        _logger.Information("Preparing A4 Document....");
                        using (PdfWriter writer = PdfWriter.GetInstance(document, fileStream))
                        {
                            document.Open();
                            document.NewPage();
                            ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(false);

                            HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                            ITagProcessorFactory tagProcessorFactory = Tags.GetHtmlTagProcessorFactory() as DefaultTagProcessorFactory;
                            htmlContext.SetTagFactory(tagProcessorFactory);

                            var pdf = new PdfWriterPipeline(document, writer);
                           
                            var html = new HtmlPipeline(htmlContext, pdf);
                            var css = new CssResolverPipeline(cssResolver, html);

                            var worker = new XMLWorker(css, true);
                            var parser = new XMLParser(worker);

                            _logger.Information("Converting HTML to PDF..");

                            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(htmlText)))
                            {
                                using (var streamReader = new StreamReader(ms))
                                {
                                    _logger.Information("Attempting to Parse HTML..");
                                    parser.Parse(streamReader);
                                    _logger.Information("Completed Parsing HTML..");
                                }
                            }
                            document.Close();
                            _logger.Information($@"Converted HTML to PDF for {fileName}");
                        }

                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(e?.Message ?? e?.InnerException?.Message);
               // throw e;
            }
        }

        private void MapFileToPath(string path, string fileName)
        {
            FilePath = Path.Combine(path, fileName);
            FileName = fileName;
        }

    }
}
