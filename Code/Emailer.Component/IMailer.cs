﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emailer.Component
{
    public interface IMailer : IDisposable
    {
        /// <summary>
        /// Recipient of the e-mail
        /// </summary>
        string To { get; set; }

        /// <summary>
        /// SMTP Email Login User
        /// </summary>
        string From { get; set; }
        /// <summary>
        /// Is the message html formatted
        /// </summary>
        bool IsHtml { get; set; }
        /// <summary>
        /// Password credentials for the SMTP Server 
        /// </summary>
        string Password { get; set; }
        /// <summary>
        /// The subject of the e-mail to be sent.
        /// </summary>
        string Subject { get; set; }
        /// <summary>
        /// STMP Host Endpoint
        /// </summary>
        string SmtpHost { get; set; }
        /// <summary>
        /// Should enable SSL for secure messages.
        /// </summary>
        bool EnableSsl { get; set; }
        /// <summary>
        /// SMTP Endpoint Port
        /// </summary>
        int SmtpPort { get; set; }
        bool SendAsync(string message);
    }
}
