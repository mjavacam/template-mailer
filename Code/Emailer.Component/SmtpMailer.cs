﻿using Enterprise.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Emailer.Component
{
    public class SmtpMailer : IMailer
    {
        private MailAddress _fromAddress;
        private ILogger _logger;
        private SmtpClient _mailerClient;
        private MailAddress _toAddress;

        public SmtpMailer(ILogger logger)
        {
            _logger = logger;
        }

        public string To { get; set; }
        public string From { get; set; }

        public bool IsHtml { get; set; }
        /// <summary>
        /// Password credentials for the SMTP Server 
        /// </summary>
        public string Password { get; set; }
        public string Subject { get; set; }
        public string SmtpHost { get; set; }
        public bool EnableSsl { get; set; }
        public int SmtpPort { get; set; }


        public bool SendAsync(string message)
        {
            bool isSent = false;
            MailMessage mailMessage = SetupSMTPMessage();

            mailMessage.Subject = Subject;
            mailMessage.Body = message;

            try
            {
                _logger.Information("Sending Email");
                _mailerClient.Send(mailMessage);
                _logger.Information("Email was sent.");
                isSent = true;
            }
            catch (Exception e)
            {
                isSent = false;
                _logger.Error(e?.Message ?? e?.InnerException.Message);
            }

            return isSent;
        }

        private MailMessage SetupSMTPMessage()
        {
            InitializeClient();

            SetupRecipients();

            AddSmtpCredentials();

            MailMessage mailMessage = new MailMessage(_fromAddress, _toAddress);
            mailMessage.IsBodyHtml = IsHtml;

            return mailMessage;
        }

        private void AddSmtpCredentials()
        {
            
            if (!string.IsNullOrEmpty(Password))
                _mailerClient.Credentials = new NetworkCredential(_fromAddress.Address, Password);
        }

        private void SetupRecipients()
        {
            _fromAddress = new MailAddress(From);
            _toAddress = new MailAddress(To);
        }

        private void InitializeClient()
        {
            _mailerClient = new SmtpClient();
            try
            {
                //_mailerClient.Host = SmtpHost;
                //_mailerClient.Port = SmtpPort;
                //_mailerClient.EnableSsl = EnableSsl;
                //_mailerClient.UseDefaultCredentials = false;
                //_mailerClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public void Dispose()
        {
            if (_mailerClient != null)
            {
                _mailerClient.Dispose();
            }
        }
    }
}
