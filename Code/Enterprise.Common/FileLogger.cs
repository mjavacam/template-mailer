﻿using Enterprise.Common;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace JNCBTemplateMailer.Logger
{
    public class FileLogger : ILogger
    {

        private string _fileName;
        private static object lockObject = new object();
        private BackgroundWorker _worker;
        public FileLogger()
            : this(HttpContext.Current.Server.MapPath($"~/Content/logs/logs-{DateTime.Now.Date.ToString("yyyy-MMM-dd")}.log"))
        {
            _worker = new BackgroundWorker();
        }
        public FileLogger(string fileName)
        {
            _worker = new BackgroundWorker();
            _fileName = fileName;
            _worker.DoWork += _worker_DoWork;
            _worker.RunWorkerCompleted += _worker_RunWorkerCompleted;
            _worker.RunWorkerAsync(_fileName);

        }

        void _worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //_fileName = e.Result as string;
            _worker.DoWork -= _worker_DoWork;
            _worker.RunWorkerCompleted -= _worker_RunWorkerCompleted;
            _worker.Dispose();
        }

        private void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var path = e.Argument as string;
            if (!string.IsNullOrWhiteSpace(path))
            {
                var dir = Path.GetDirectoryName(path);

                if (!Directory.Exists(dir))
                {
                    var dirCreated = Directory.CreateDirectory(dir);
                }

                e.Result = path;
            }

            // e.Cancel = true;
        }

        public void Log(string type, string message)
        {
            var formattedLog = string.Format(@"{0} - [{1}] - {2}", DateTime.Now, type.ToUpper(), message);
            try
            {
                if (!string.IsNullOrWhiteSpace(_fileName))
                {
                    using (StreamWriter writer = File.AppendText(_fileName))
                    {
                        lock (lockObject)
                        {
                            writer.Write(formattedLog);
                            writer.Write("\r\n");
                            writer.Flush();
                            writer.Close();
                        }
                    }
                    return;
                }
                Debug.WriteLine("APPENDING LOGS FAILED - FILEPATH NULL " + DateTime.Now, "INTERNAL");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Developer " + ex.ToString());
                throw ex;
            }

        }

        public void Information(string info)
        {
            Log("info".ToUpper(), info);
        }

        public void Warning(string warning)
        {
            Log("warning".ToUpper(), warning);
        }

        public void Error(string error)
        {
            Log("error".ToUpper(), error);
        }
    }
}