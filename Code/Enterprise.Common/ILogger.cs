﻿namespace Enterprise.Common
{
    public interface ILogger
    {
        void Error(string message);
        void Information(string message);
        void Warning(string message);
        void Log(string type, string message);
    }
}
