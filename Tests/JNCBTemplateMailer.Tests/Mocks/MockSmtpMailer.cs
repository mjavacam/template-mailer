﻿using Emailer.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Common;

namespace JNCBTemplateMailer.Tests.Mocks
{
    public class MockSmtpMailer : IMailer
    {
        public MockSmtpMailer(ILogger logger) 
        {
            IsHtml = true;
            From = "support@agilelab.com";
            To = "mjavacam2010@gmail.com";
            Subject = "Test";
            SmtpHost = "localhost";
            SmtpPort = 25;
        }

        public string To { get; set; }
        public string From { get; set; }
        public bool IsHtml { get; set; }
        public string Password { get; set; }
        public string Subject { get; set; }
        public string SmtpHost { get; set; }
        public bool EnableSsl { get; set; }
        public int SmtpPort { get; set; }

        public bool SendAsync(string message)
        {
            bool returnValue = false;
            if (string.IsNullOrWhiteSpace(To) || string.IsNullOrWhiteSpace(From) || string.IsNullOrWhiteSpace(SmtpHost))
            {
                returnValue = false;
                throw new ArgumentNullException();
            }
            returnValue = true;
            return returnValue;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~MockSmtpMailer() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}
