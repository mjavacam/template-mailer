﻿using Emailer.Component;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shouldly;
using JNCBTemplateMailer.Tests.Mocks;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using JNCBTemplateMailer.Logger;

namespace JNCBTemplateMailer.Tests
{


    [TestFixture]
    public class EmailerComponentTests
    {
        private FileLogger logger;

        [SetUp]
        public void SetUp()
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        

            logger = new FileLogger($@"{path}/logs/logs-{DateTime.Now.Date.ToString("yyyy - MMM - dd")}.log");
        }


        [Test]
        public void ShouldSendEmailWhenNotGivenCredentials_ReturnsTrue()
        {
            IMailer mailer = new MockSmtpMailer(logger);

            var actual = mailer.SendAsync(";Test");

            var expected = true;
            actual.ShouldBe(expected);
        }

        [Test]
        public void ShouldNotSendEmailWhenNotGivenHostAndPort_ReturnsFalse()
        {

            IMailer mailer = new MockSmtpMailer(logger);
            mailer.SmtpHost = null;

            bool actual = true;
            Assert.Throws<ArgumentNullException>(() => actual = mailer.SendAsync("Testing Throws"));
        }



    }
}
